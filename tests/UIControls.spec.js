const {test,expect} = require('@playwright/test');



test('Dropdown Test', async({browser})=>
{
    //chrome - plugin/cookies
        const context = await browser.newContext(); //browser instance is open without cookies
        const page = await context.newPage(); // creates actual page to start your automation
        await page.goto("https://www.rahulshettyacademy.com/loginpagePractise/");
       

       await page.locator("#username").fill("rahulshetty");
       const dropdown = await page.locator("select.form-control");
       await dropdown.selectOption("consult"); 
    
       //radiobutton
       await page.locator(".radiotextsty").last().click(); //radiobutton last user
       await page.locator("#okayBtn").click(); //okay button popup

       await page.locator("#signInBtn").click();

       console.log(await page.locator(".radiotextsty").last().isChecked());
       //not checked output will print as false: have to see logs
       await expect(await page.locator(".radiotextsty").last()).toBeChecked();
       //not checked it will kickoff because it is asssertion
       //Checkbox
       await page.locator("#terms").click();
       await expect (page.locator("#terms")).toBeChecked();
       //await page.locator("#terms").uncheck();
       //expect(await page.locator("#terms").isChecked().toBeFalsy();

       await page.pause();
}
);



