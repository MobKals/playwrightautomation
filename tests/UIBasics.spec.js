const {test,expect} = require('@playwright/test')



test.only('Browser Playwright Test', async({browser})=>
{
    //chrome - plugin/cookies
        const context = await browser.newContext(); //browser instance is open without cookies
        const page = await context.newPage(); // creates actual page to start your automation

        const userName = page.locator("#username");
        const password = page.locator("#password");
        const signIn = page.locator("#signInBtn");
        const cardTitles = page.locator(".card-body a");

        await page.goto("https://www.rahulshettyacademy.com/loginpagePractise/");
        console.log(await page.title());

        //login
        await page.locator("#username").type("Kalyanin");
        await page.locator("#password").type("learning");
        await page.locator("#signInBtn").click();
        //wait until this locator shows up
        console.log(await page.locator("[style*='block']").textContent());
        //await expect(locator).toContainText('substring');
        await expect(page.locator("[style*='block']")).toContainText('Incorrect');
       
        //type - fill
        await userName.fill("rahulshettyacademy");
        await password.fill("learning");
        // race condition: it will try to execute click before it is happing
        // it will apply to click only
        await Promise.all(
                [
                    page.waitForNavigation(),
                   signIn.click()
                ]
               )
        

        //to get element from list

       // console.log(await page.locator(".card-body a").first().textContent());
        // console.log(await page.locator(".code-body a").nth(1).textContent());
        // console.log(await page.locator(".code-body a").last().textContent());
        const allTitles = await cardTitles.allTextContents();
        console.log(allTitles);

});
test('Page Playwright Test', async ({page})=>
{
        await page.goto("https://www.google.com");
        //get title
        console.log(await page.title());
        await expect (page).toHaveTitle("Google");

}); 