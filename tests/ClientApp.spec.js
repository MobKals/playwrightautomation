const {test,expect} = require('@playwright/test');



test('Browser context-validating Error login Test', async({browser})=>
{
    //chrome - plugin/cookies
        const context = await browser.newContext(); //browser instance is open without cookies
        const page = await context.newPage(); // creates actual page to start your automation
        await page.goto("https://www.rahulshettyacademy.com/client/");
       // register on site login to application and get the title
       //wait mechanism service based :wait until network is in idle state
       //how to check go to console network api calls and responce :
       //await page.waitForLoadState('networkidle') ;

       await page.locator("#userEmail").fill("ashika@gmail.com");
       await page.locator("#userPassword").type("IamKing000");
       await page.waitForLoadState('networkidle')
       await page.locator("[value='Login']").click();
       await page.waitForLoadState('networkidle');

       const titles = await page.locator(".card-body b").allTextContents();
       console.log(titles);
       //  if you want to run a single test either you chane in config file. or use single .only infront of test or runtime
       // you say npx playwright test ClientApp.spec.js

       //non service oriented , before click() tell to be alert
       //await Promise.all();
       //await page.waitForNavigation();
       

})
